import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { text } from '@storybook/addon-knobs';
import  SimpleButton  from './SimpleButton';


storiesOf('Button', module)
  .add('with text and knobs', () => <SimpleButton
    onClick={action('clicked')}
    label={text("Label", "Click me!")} />)
  .add('with some emoji and knobs', () => (
    <SimpleButton
      onClick={action('clicked')}
      label={<span role="img" aria-label="so cool">
        😀 😎 👍 💯
  </span>} />
  ));
