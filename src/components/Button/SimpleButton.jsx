import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
//import PropTypes from 'prop-types';

class SimpleButton extends Component {

    render() {
        const { label } = this.props;

        return (
            <Button>
                <span>{label}</span>
            </Button>
        );
    };
}

SimpleButton.propTypes = {
    label: PropTypes.string,
};

export default SimpleButton;