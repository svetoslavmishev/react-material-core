import { configure, addDecorator, addParameters } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';


addDecorator(withKnobs);
addParameters({ viewports: 'responsive' });


const req = require.context('../src', true, /\.story\.js$/);

function loadStories() {

  // You can require as many stories as you need.
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
